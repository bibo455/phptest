<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular.min.js"></script>
</head>
<!DOCTYPE html>


<body>

<div ng-app="calc" ng-controller="cal">
First Number : <input name="num1" ng-model="num1"/><br>
Second Number : <input name="num2" ng-model="num2"/><br>
{{num1+ " + " + num2}} = {{ sum() }} <br>

{{num1+ " - " + num2}} = {{ sub() }} <br>

{{num1+ " x " + num2}} = {{ multi() }} <br>

{{num1+ " / " + num2}} = {{ div() }} <br>

{{num1+ " mod " + num2}} = {{ mod() }} <br>

</div>

<script>
var app = angular.module("calc", []);
app.controller("cal", function($scope) {
    $scope.num1 = 5;
    $scope.num2 = 5;
    $scope.sum = function(){
      return parseInt($scope.num1)+parseInt($scope.num2);
    };
    $scope.sub = function(){
      return parseInt($scope.num1)-parseInt($scope.num2);
    };
    $scope.multi = function(){
      return parseInt($scope.num1)*parseInt($scope.num2);
    };
    $scope.div = function(){
      return parseInt($scope.num1)/parseInt($scope.num2);
    };
     $scope.mod = function(){
      return parseInt($scope.num1)%parseInt($scope.num2);
    };
});
</script>

</body>
</html>

